using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KBZ.Domain.Data.Entities
{
    [Table("User")]
    public class User
    {
        [Key]
        [Column("UserId")]
        public string UserId { get; set; }

        [Column("Password")]
        public string Password { get; set; }
    }
}
