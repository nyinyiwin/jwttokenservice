using System;

namespace KBZ.Domain.Data.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        //IUserRepository Users { get; }
        int Complete();
    }
}