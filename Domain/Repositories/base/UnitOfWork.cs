﻿

using KBZ.Domain.Data.Interfaces;

namespace KBZ.Domain.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataBaseContext _context;
        //public IUserRepository Users { get; private set; }

        public UnitOfWork(DataBaseContext context)
        {
            _context = context;
            //Users = new UserRepository(_context);
        }
        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}