﻿using KBZ.Domain.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KBZ.Domain.Data.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        
    }
}
