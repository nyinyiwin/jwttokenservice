using KBZ.Domain.Data.Entities;
using KBZ.Domain.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace KBZ.Domain.Data.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DataBaseContext context) 
            : base(context)
        {
            var found =  context.Users.Where(o => o.UserId.Equals("user")).FirstOrDefault();
            if(found == null)
            {
                context.Users.Add(new User{UserId="user",Password="pass1"});
                context.SaveChanges();
            }
            
        }
    }
}