namespace KBZ.Models {     
    public class Message 
    {        
        public string ResponseCode { get; set; }        
        public string ResponseDescription { get; set; }
    }

}