using System.Collections.Generic;
using KBZ.Models;
namespace KBZ.Models {     
    public class UserCredentialRequest 
    {        
        public string UserId { get; set; }        
        public string Password { get; set; }
    }

    public class UserCredentialResponse
    {        
        public string Token { get; set; }
        public IList<Message> Response{ get; set; }
    }

}