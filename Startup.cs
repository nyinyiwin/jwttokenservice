﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using KBZ.Domain.Data;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
//using Swashbuckle.AspNetCore.Swagger;
using KBZ.Business;
using KBZ.Business.Interfaces;
using KBZ.Domain.Data.Interfaces;
using KBZ.Domain.Data.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using KBZ.Helpers;
using System.Text;
using NSwag.AspNetCore;
using NSwag.SwaggerGeneration.Processors;
using NSwag.SwaggerGeneration.WebApi;
using System.Reflection;

namespace JWTTokenService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddHealthChecks();
            services.AddAutoMapper();
            services.AddDbContext<DataBaseContext>(opt => opt.UseInMemoryDatabase("User"));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            //services.AddSwaggerDocument();
            //services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new Info { Title = "KBZ API", Version = "1.0" }); });

            // configure strongly typed settings objects
            var jwtSettingsSection = Configuration.GetSection("JWTSettings");
            services.Configure<JWTSettings>(jwtSettingsSection);
            var jwtSettings = jwtSettingsSection.Get<JWTSettings>();
            var SecretKey = Encoding.ASCII.GetBytes(jwtSettings.Secret);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(SecretKey),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            ConfigureOpenApi(services);
            ConfigureVersioning(services);
            ConfigureDependencyInjection(services);
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
                app.UseHttpsRedirection();
            }

            app.UseAuthentication();

            app.UseSwaggerUi3(typeof(Startup).GetTypeInfo().Assembly, settings =>
                {
                    settings.DocumentPath = "/swagger/v1/swagger.json";
                    settings.EnableTryItOut = true;
                    settings.DocExpansion = "list";
                    settings.PostProcess = document =>
                    {
                        document.BasePath = "/";
                    };
                    settings.GeneratorSettings.Description = "Agents";
                    settings.GeneratorSettings.Title = "KBZ API";
                    settings.GeneratorSettings.Version = "1.0";
                    settings.GeneratorSettings.OperationProcessors.Add(
                        new ApiVersionProcessor() { IncludedVersions = new[] { "1.0" } }
                    );
                });

            // app.UseSwagger();
            // app.UseSwaggerUI(c =>
            // {
            //     c.SwaggerEndpoint("/swagger/v1.0/swagger.json", "KBZ API V1");
                
            // });
            // app.UseSwaggerUi3(typeof(Startup).GetTypeInfo().Assembly, settings =>
            //     {
            //         settings.DocumentPath = "/swagger/v1/swagger.json";
            //         settings.EnableTryItOut = true;
            //         settings.DocExpansion = "list";
            //         settings.PostProcess = document =>
            //         {
            //             document.BasePath = "/";
            //         };
            //         settings.GeneratorSettings.Description = "KBZ Web API v1.0";
            //         settings.GeneratorSettings.Title = "KBZ API";
            //         settings.GeneratorSettings.Version = "1.0";
            //         settings.GeneratorSettings.OperationProcessors.Add(
            //             new ApiVersionProcessor() { IncludedVersions = new[] { "1.0" } }
            //         );
            //     });


            app.UseMvc();
            app.UseHealthChecks("/healthcheck");
        }


        private void ConfigureVersioning(IServiceCollection services)
        {
            services.AddApiVersioning(options =>
            {
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.AssumeDefaultVersionWhenUnspecified = true;
                // Includes headers "api-supported-versions" and "api-deprecated-versions"
                options.ReportApiVersions = true;
            });
        }
        private void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddTransient<IUserBiz, UserBiz>();
            services.AddTransient<IMapper, Mapper>();
            services.AddTransient<IUserRepository, UserRepository>();
        }
        private void ConfigureOpenApi(IServiceCollection services)
        {
            services.AddSwaggerDocument();
        }
    }
}
