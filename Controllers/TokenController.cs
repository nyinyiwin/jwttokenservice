﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KBZ.Business.Interfaces;
using KBZ.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace JWTTokenService.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Produces("application/xml", "application/json")]
    public class TokenController : ControllerBase
    {
        Message BadUserCredential = new Message{ ResponseCode="100202",ResponseDescription="Unauthorized user." };
        Message Success = new Message{ ResponseCode="100201",ResponseDescription="Success" };
        Message Fail = new Message{ ResponseCode="100203",ResponseDescription="An error had occured." };

        private readonly IUserBiz _usrCtrl;
        
        public TokenController(IUserBiz usrCtrl)
        {
            _usrCtrl = usrCtrl;
        }

        // POST api/values
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Generate([FromBody]UserCredentialRequest  param)
        {
            try 
            {
                if(_usrCtrl.IsAuthorized(param)) 
                {
                    UserCredentialResponse obj = _usrCtrl.GetCredential(param);
                    obj.Response =  new List<Message> { Success };
                    return Ok(obj);
                }    
                else
                {
                    return BadRequest(new UserCredentialResponse { Response = new List<Message> { BadUserCredential }});
                }
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new UserCredentialResponse { Response = new List<Message> { Fail }});
            }
            
        }

        [AllowAnonymous]
        public async Task<IActionResult> Gets(string name)
        {
            return Ok();
        }

    }
}
