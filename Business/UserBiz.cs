using KBZ.Domain.Data.Entities;
using System;
using System.Collections.Generic;
using KBZ.Business.Interfaces;
using KBZ.Domain.Data.Interfaces;
using System.Linq;
using KBZ.Models;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using KBZ.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace KBZ.Business
{
    public class UserBiz : IUserBiz
    {
        private readonly JWTSettings _jwtSettings;
        private readonly IMapper _mapper;
        private readonly IUserRepository _repos;
        public UserBiz(IUserRepository repos,IMapper mapper,IOptions<JWTSettings> jwtSettings)
        {
            _repos = repos;
            _mapper = mapper;
            _jwtSettings = jwtSettings.Value;
        }
        public bool IsAuthorized(UserCredentialRequest param){
            try{
                    User user =_mapper.Map<User>(param);
                    User obj = _repos.Find(o=> o.UserId.Equals(user.UserId)).FirstOrDefault();
                    if(obj.Password == param.Password)
                        return true;
                    else return false;          
            }
            catch{ return false; }
        }
        
        public UserCredentialResponse GetCredential(UserCredentialRequest param)
        {
            string token = GenerateToken(param.UserId);
            return new UserCredentialResponse{ Token = token};
        }

        string GenerateToken(string UserId)
        {
            string result = string.Empty;
            var tokenHandler = new JwtSecurityTokenHandler();
            var SecretKey = Encoding.ASCII.GetBytes(_jwtSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, UserId)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(SecretKey), SecurityAlgorithms.HmacSha256Signature)
            };
            
            var token = tokenHandler.CreateToken(tokenDescriptor);
            result = tokenHandler.WriteToken(token);
            return result;
        }
    }
}