using KBZ.Domain.Data.Entities;
using KBZ.Models;
using System;
using System.Collections.Generic;

namespace KBZ.Business.Interfaces
{
    public interface IUserBiz
    {
       bool IsAuthorized(UserCredentialRequest param);
       UserCredentialResponse GetCredential(UserCredentialRequest param);
    }
}